const mongoose = require('mongoose');

module.exports = {
    connect:function connect(client,isCloud=true,address=null,port=null){

        const stringDBConnection = isCloud ? `mongodb+srv://${client.userDB}:${client.passwordDB}@cluster0.9v77a.mongodb.net/${client.database}?retryWrites=true&w=majority`
        : `mongodb://${address}:${port}/${client.database}`;

        mongoose.connect(stringDBConnection,{
        useNewUrlParser: true,
        useUnifiedTopology: true})
        .then(()=> console.log("Connexion à la base MongoDB réussie"))
        .catch((error)=> console.log(`Connexion à la base MongoDB échouée : ${error}`)
    )},
    DatabaseClient:class DatabaseClient{
        constructor(user,password,database){
            this.userDB = user;
            this.passwordDB = password;
            this.database = database;
        }
    }
}