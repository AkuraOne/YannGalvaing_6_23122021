const mongoose = require('mongoose');

const userSchema = mongoose.Schema({
    email:{type: String, description: "adresse email de l'utilisateur", require: true, unique: true},
    password: {type: String, description:"Mot de passe de l'utilisateur haché", require: true}
});





module.exports = mongoose.model('User',userSchema);