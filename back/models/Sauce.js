const mongoose = require('mongoose');

const sauceSchema = mongoose.Schema({
    userId: {type: String, description: "Identifiant MongoDB unique de l'utilisateur qui a créé la sauce"},
    name: {type: String, description: "Nom de la sauce", require: true},
    manufacturer:{type: String, description: "Fabriquant de la sauce"},
    description:{type: String, description: "Description de la sauce"},
    mainPepper: {type: String, description: "Le principal ingrédient épicé de la sauce"},
    imageUrl: {type: String, description: "URL de l'image de la sauce téléchargée"},
    heat: {type: Number, description: "note entre 1 et 10 décrivant la sauce"},
    likes: {type: Number, description: "Nombre d'utilisateurs qui aiment la sauce"},
    dislikes: {type: Number, description: "Nombre d'utilisateurs qui n'aiment pas la sauce"},
    usersLiked: {type: ["String <userId>"], description: "Collection d'identifiants utilisateurs qui ont aimé la sauce"},
    usersDisliked: {type: ["String <userId>"], description: "Collection d'identifiants utilisateurs qui n'ont pas aimé la sauce"}
});

module.exports = mongoose.model('Sauce',sauceSchema);