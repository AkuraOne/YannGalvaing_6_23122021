const router = require('express').Router();//router permettant de gérer les routes
const sauceCtrl = require('../controllers/sauce');//controleur pour la gestion des sauces
const auth = require('../middleware/auth');//middleware controlant l'authentification
const multer = require('../middleware/multer_config');//middleware gérant les fichiers images entrants
const {forbiddenValuesValidation} = require('../middleware/validator');

//route d'obtention des sauces
router.get('/',auth,sauceCtrl.getSauces);

//route d'obtention d'une sauce
router.get('/:id',auth,sauceCtrl.getOneSauce);

//route d'ajout de sauce
router.post('/',auth,forbiddenValuesValidation,multer,sauceCtrl.addSauce);

//route d'ajout d'un like'
router.post('/:id/like',auth,multer,sauceCtrl.addLikeSauce);

//route de mise à jour d'une sauce
router.put('/:id',auth,forbiddenValuesValidation,multer,sauceCtrl.updateSauce);

//route de suppression d'une sauce
router.delete('/:id',auth,sauceCtrl.deleteSauce);

module.exports=router;