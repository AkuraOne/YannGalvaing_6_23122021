const router = require('express').Router();//routeur permettant de gérer les routes
const userCtrl = require('../controllers/user');//controle permettant la gestion des utilisateurs
const {loginInputsValidation} = require('../middleware/validator');//sécurisation des entrées concernant la gestion des utilisateurs

//routes concernant la connexion et l'inscription
router.post('/signup', loginInputsValidation,userCtrl.signup);
router.post('/login', loginInputsValidation, userCtrl.login);


module.exports = router;