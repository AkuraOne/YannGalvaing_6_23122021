const Sauce = require('../models/Sauce');
const fs = require('fs');

//fonction d'obtention des sauces
exports.getSauces = async (req,res,next)=> {
    try{
        console.log("Demande des sauces...");
        const sauces = await Sauce.find();

        if(!sauces){
            return res.status(404).json({message: "Aucune sauce dans la base de données"})
        }

        res.status(200).json(sauces);
    }
    catch(error){
        res.status(400).json({error});
    }
};

//fonction d'obtention d'une sauce
exports.getOneSauce = async (req,res,next)=> {
    try{
        console.log("Demande d'une sauce...");
        const sauce = await Sauce.findOne({_id: req.params.id});

        if(!sauce){
            return res.status(404).json({message: "Sauce non trouvée"});
        }

        res.status(200).json(sauce);
    }
    catch(error){
        res.status(400).json({error});
    }
};

//fonction d'ajout d'une sauce
exports.addSauce = async (req,res,next) =>{
    console.log("Ajout d'une sauce...");
    const sauceObject = JSON.parse(req.body.sauce);

    delete sauceObject._id;

    const sauce = new Sauce({
        ...sauceObject,
        imageUrl: `${req.protocol}://${req.get('host')}/images/${req.file.filename}`,
        likes: 0,
        dislikes: 0,
        usersLiked: [],
        usersDisliked: []
    });

    try{
        await sauce.save();
        res.status(201).json({message:"Sauce créée"});
    }
    catch(error){
        res.status(400).json({error});
    }
};

//fonction d'ajout d'un like à une sauce
exports.addLikeSauce = async (req,res,next)=>{
    try{
        const userId = req.body.userId;
        const likeObject = req.body.like;
        const sauceId = req.params.id;

        if(!userId || !sauceId){
            return res.status(400).json({message:"Mauvaise authentification ou sauce indéterminé"});
        }

        const sauce = await Sauce.findOne({_id: sauceId});

        if(!sauce){
            return res.status(404).json({message: "Sauce non trouvée"});
        }

        switch(likeObject){
            case 0:
                console.log("Annulation d'une approbation ...");
                await cancelLikeDislike(sauce,userId,res);
            break;
            case 1:
                console.log("ajout d'une approbation ...");
                await like(sauce,userId,res);
            break;
            case -1:
                console.log("Ajout d'une désapprobation ...");
                await dislike(sauce,userId,res);
            break;
            default:
                console.log("Annulation d'une approbation ...");
                await cancelLikeDislike(sauce,userId,res);
            break;
        }
    }
    catch(error){
        res.status(500).json({error});
    }
};

//fonction de mise à jour d'une sauce
exports.updateSauce = async (req,res,next) =>{
    console.log("Mise à jour d'une sauce");

    try{
        const oldSauce = await Sauce.findOne({_id: req.params.id});

        const newSauce = req.file ? {
            ...JSON.parse(req.body.sauce), 
            _id:req.params.id, 
            imageUrl: `${req.protocol}://${req.get('host')}/images/${req.file.filename}`
        }
        :{
            ...req.body,_id:req.params.id
        };

        console.log(newSauce.imageUrl);
        
        if(!oldSauce){
            return res.status(404).json({message: "Sauce non trouvée"});
        }

        if(req.auth.userId != oldSauce.userId){
            return res.status(401).json({message: "Requête non autorisée"});
        }

        //suppression de l'ancien fichier image de la sauce
        if(req.file){
            const oldFileName = oldSauce.imageUrl.split('/images/')[1];
            fs.unlink(`images/${oldFileName}`,(error,req)=>{
                if(error){
                    console.log(`${error} 
                    \nErreur de suppression ancienne image : ${oldFileName}`);
                }
                else{
                    console.log("Ancienne image supprimée");
                }
            });
        }

        try{
            await Sauce.updateOne({_id: req.params.id},newSauce);
            res.status(201).json({message:"Sauce mise à jour"});
        }
        catch(error){
            return res.status(400).json({error});
        }
    }
    catch(error){
        res.status(500).json({error});
    }
};

//Fonction de suppression d'une sauce
exports.deleteSauce = async (req,res,next) =>{
    const sauceToDelete = await Sauce.findOne({_id:req.params.id});

    if(sauceToDelete.userId != req.auth.userId){
        return res.status(401).json({message: "Requête non autorisée"});
    }

    try{
        await Sauce.deleteOne({_id: req.params.id});
        res.status(200).json({message:"Sauce supprimée"});

        //suppression de l'ancien fichier image de la sauce
        fs.unlink(`images/${sauceToDelete.imageUrl.split('images/')[1]}`,(error)=>{
            if(error){
                console.log(error);
            }
            else{
                console.log("Image de la sauce supprimée");
            }
        });
    }
    catch(error){
        res.status(400).json({error});
    }
};

//fonction d'indexation d'un string dans un tableau de string
//-1 si pas dans le tableau sinon index de 0 à ... si existe
function arrayIndexOf(array,stringToFind){
    
    if(array.length == 0) return -1;

    for(let i = 0; i < array.length; i++){
        
        if(stringToFind == array[i]){
            return i;
        }
        
        if(i == (array.length -1)){
            return -1;
        }
    }
}

//fonction gérant le like
async function like(sauce, userId,res){
    try{
        let indexLiked = arrayIndexOf(sauce.usersLiked,userId);

        if(indexLiked > -1){
            return res.status(204).json({message: "Vous avez déjà approuvé"});
        }

        sauce.likes += 1;
        sauce.usersLiked.push(userId);
        
        await Sauce.updateOne({_id: sauce._id},sauce);
        return res.status(201).json({message: "Vous avez approuvé"});
    }
    catch(error){
        res.status(500).json({error});
    }
}

//fonction gérant le dislike
async function dislike(sauce, userId,res){
    try{
        let indexDisliked = arrayIndexOf(sauce.usersDisliked,userId);
        
        if(indexDisliked > -1){
            return res.status(204).json({message: "Vous avez déjà désapprouvé"});
        }

        sauce.dislikes += 1;
        sauce.usersDisliked.push(userId);

        await Sauce.updateOne({_id: sauce._id},sauce);
        return res.status(201).json({message: "Vous avez désapprouvé"});
    }
    catch(error){
        res.status(500).json({error});
    }
}

//fonction gérant l'annulation d'un like ou un dislike
async function cancelLikeDislike(sauce,userId,res){
    try{
        let indexLiked = arrayIndexOf(sauce.usersLiked,userId);
        let indexDisliked = arrayIndexOf(sauce.usersDisliked,userId);

        if(indexLiked >-1){
            sauce.likes -= 1;
            sauce.usersLiked.splice(indexLiked,1);
        }

        if(indexDisliked >-1){
            sauce.dislikes -= 1;
            sauce.usersDisliked.splice(indexDisliked,1);
        }

        await Sauce.updateOne({_id: sauce._id},sauce);
        res.status(201).json({message: "Votre approbation a été annulée"});
    }
    catch(error){
        res.status(500).json({error});
    }
}