const User = require('../models/User');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');

//fonction d'inscription
exports.signup = async (req,res,next)=>{
    try{
        //cryptage du mot de passe avec bcrypt en ajoutant du salage
        const hashedPass = await bcrypt.hash(req.body.password,10);

        const user = new User({
            email: req.body.email,
            password: hashedPass
        });
         
        try{
            await user.save();
            res.status(201).json({message:"User created !"});
        }
        catch(error){
            res.status(400).json({error});
        }
        
    }
    catch(error){
        res.status(500).json({error});
    }
}

//fonction de connexion
exports.login = async (req,res,next)=>{
    try{
        const user = await User.findOne({email: req.body.email});

        if(!user){
            return res.status(404).json({error: "No user finded !"});
        }

        //si le mot de passe est correct on donne un jeton à l'utilisateur
        if(await bcrypt.compare(req.body.password,user.password)){
            const token = jwt.sign({userId:user._id},'RANDOM_ACCESS_TOKEN_SECRET',{expiresIn:'12h'});

            res.status(200).json({
                userId: user._id,
                token:token
            });
        }
        else{
            return res.status(400).json({message:"Incorrects login or password !"});
        }
    }
    catch(error){
        res.status(500).json({error});
    }
}