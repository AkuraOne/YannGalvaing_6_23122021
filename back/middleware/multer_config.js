const multer = require('multer');

//définition des types mime pour extraction de l'extension du fichier
const MIME_TYPES = {
    'image/png': 'png',
    'image/jpg': 'jpg',
    'image/jpeg': 'jpg',
    'image/x-icon': 'ico',
    'image/webp': 'webp',
    'image/svg+xml': 'svg',
    'image/gif': 'gif',
    'image/bmp': 'bmp'
};

const diskStorage = multer.diskStorage({
    destination: (req,file,callback)=>{
        callback(null,'images');//répertoire images
    },
    filename: (req,file,callback)=>{
        //Remplacement des white spaces, pouvant être problématiques sur serveur, par des underscores, puis en splitant l'extension on insert la date
        const name = file.originalname.split(' ').join('_').split(`.${MIME_TYPES[file.mimetype]}`).join("") + `_${Date.now()}.${MIME_TYPES[file.mimetype]}`;
        callback(null,name);
    }
});

module.exports = multer({storage: diskStorage}).single('image');
