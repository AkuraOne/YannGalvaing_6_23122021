const jwt = require('jsonwebtoken');

module.exports = (req,res,next)=>{
    const token = req.headers.authorization.split('Bearer ')[1];
    
    try{
        const decodedToken = jwt.verify(token, 'RANDOM_ACCESS_TOKEN_SECRET');
        const userId = decodedToken.userId;
        req.auth = {userId};
        if(req.body.userId && req.body.userId != userId){
            throw 'Unauthorized ID';
        }
        else{
            next();
        }
    }
    catch(error){
        res.status(401).json({error: error | 'Requête non authentifiée'});
    }
}