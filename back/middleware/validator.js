const { body, validationResult } = require('express-validator');

const testForbiddenValues = async (req,fieldString)=>{
    await body(fieldString).not().custom((value)=>{
        return /[\^\[\]<>$%/|#&`{}]/.test(value);
    }).withMessage(`${fieldString}`+" don't must contain characters forbidden => ^[]<>$%/|#&`{}").run(req);
};

const validatedResult = (req,res,next) =>{
    const errors = validationResult(req);
    
    if(!errors.isEmpty()){
        return res.status(400).json({errors: errors.array()});
    }
    else{
        next();
    }
};

exports.loginInputsValidation = async (req,res,next)=>{
    await body('email').isEmail().withMessage("Must be a valid email !").run(req);
    await testForbiddenValues(req,'password');

    validatedResult(req,res,next);
};

exports.forbiddenValuesValidation = async (req,res,next)=>{
    await testForbiddenValues(req,'name');

    await testForbiddenValues(req,'manufacturer');

    await testForbiddenValues(req,'description');

    await testForbiddenValues(req,'mainPepper');

    validatedResult(req,res,next);
};
