const express = require('express');
const app = express();
const dotenv = require('dotenv');
const db = require('./connection/database');
const path = require('path');
const sauceRoutes = require('./routes/sauce');
const userRoutes = require('./routes/user');

//Chargement des paramètres d'environnement de NodeJS
dotenv.config();
const userDB = process.env.MONGO_USER;
const userPwd = process.env.MONGO_USER_PASSWORD;
const database = process.env.MONGO_DB;

app.use(express.json());


//Connexion à la base de données MongoDB test du projet
const clientDB = new db.DatabaseClient(userDB,userPwd,database);
db.connect(clientDB);

//Accès CORS Cross Origin Resource Sharing
app.use((req,res,next)=>{
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Headers','Content,Content-Type,Origin,Accept,Authorization,X-Requested-With');
    res.setHeader('Access-Control-Allow-Methods','GET,POST,PUT,PATCH,OPTIONS,DELETE');
    next();
});

//routes de base
app.use('/images',express.static(path.join(__dirname,'images')));
app.use('/api/sauces',sauceRoutes);
app.use('/api/auth',userRoutes);



module.exports = app;
